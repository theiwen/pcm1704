--------------------------------------------------------------------------------
-- Engineer: Klimann Wendelin
--
-- Create Date:   14:54:40 16/Apr/2014
-- Design Name:   PSM1704
-- Description:   
-- 
-- VHDL Test Bench for module: PCM1704
--
-- version: 00.01
--
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

ENTITY PCM1704_tb_vhd IS
END PCM1704_tb_vhd;

ARCHITECTURE behavior OF PCM1704_tb_vhd IS 
	constant width : integer := 32;
	-- Component Declaration for the Unit Under Test (UUT)
	COMPONENT PCM1704_logic
	generic(width : integer := width);
	PORT(
		CLK         : IN std_logic;
		WCLK        : OUT std_logic;
		BCLK        : OUT std_logic;
		DOUT        : OUT std_logic;
		RESET       : IN std_logic;          
		DATA_in     : IN std_logic_vector(23 downto 0);
		--DATA_RDY_out   : OUT std_logic;
		DATA_RDY_in    : IN std_logic		
		);
	END COMPONENT;

	--Inputs
	SIGNAL CLK      :  std_logic := '0';
	SIGNAL RESET    :  std_logic := '0';

	--Outputs
	SIGNAL WCLK     :  std_logic := '0';
	SIGNAL BCLK     :  std_logic := '0';
	SIGNAL DOUT     :  std_logic := '0';
	SIGNAL DATA_in     :  std_logic_vector(23 downto 0);
	SIGNAL DATA_RDY_in    :  std_logic;

BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: PCM1704_logic 
	PORT MAP(
		CLK         => CLK,
		WCLK        => WCLK,
		BCLK        => BCLK,
		DOUT        => DOUT,
		RESET       => RESET,
		DATA_in     => DATA_in,
		DATA_RDY_in    => DATA_RDY_in
	);
	
	-- creates a reset signal at the start of the sequence 
	p_reset : process
	begin
		RESET <= '1';
		--LR_CK <= '1';
		wait for 600 ns;
		RESET <= '0';
		-- Reset finished
		wait;
	end process	p_reset;
	
	-- generates the bit clock signal
	p_clk : process
	begin
		CLK <= '0';
		wait for 10 ns;
		CLK <= '1';
		wait for 10 ns;
	end process p_clk;

	
	
	-- provides the parallel input signal and the corresponding DATA_RDY_in 
	p_dout : process
	variable i : POSITIVE :=1;
	begin
		wait for 10 ns;
	   DATA_RDY_in <= '1';
		wait for 620 ns;
		--DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "111111111111111111111111";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "001100000000000000000000";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		--DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "110011001100110011001100";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "101010101010101010101010";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		--DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "110011001100110011001100";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "110011000000000000000011";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		--DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "001100000000000000000000";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "111111111111111111111111";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		--DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "001100000000000000000000";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "110011001100110011001100";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		--DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "101010101010101010101010";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "110011001100110011001100";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		--DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "110011000000000000000011";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		DATA_RDY_in <= '0';
		wait for 10 ns;
		DATA_in <= "001100000000000000000000";
		wait for 10 ns;
		DATA_RDY_in <= '1';
		wait for 620 ns;
		--DATA_RDY_in <= '0';
	end process p_dout;

END;
