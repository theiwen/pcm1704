---------------------------------------------------------------------------------
-- Engineer:      Klimann Wendelin
--
-- Create Date:   14:14:20 16/Apr/2014
-- Design Name:   PCM1704
--
-- Description:   
-- 
-- This module provides a bridge between an serial audio ADC (PCM1704)
-- and a parallel device (microcontroller, IP block).
--
--
-- Input takes:
-- -Clock   (CLK)
-- -DATA parallel input
-- -DATA_RDY input ready signal.
--
-- Output provides:
-- -Bit Clock   (BCLK)
-- -Word Clock  (WCLK)
-- -Serial Data (DOUT)
-- 
--
-- The data from the parallel input is shifted to the I2S data output
-- but just the last 24 Bits before the falling edge from WCLK are used
-- for the conversion.
--
--------------------------------------------------------------------------------
-- I2S Waveform summary
--
-- BCLK       __    __   __    __    __            __    __    __    __   
--           | 1|__| 2|_| 3|__| 4|__| 5|__... ... |32|__| 1|__| 2|__| 3| ...
--
-- WCLK                                   ... ...  __
--           ___________________Data______________|  |______________Data ...
--
-- DATA      x< 00 ><D24><D22><D21><D20>  ... ...     < 00 ><D24><D23>   ...
--
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity PCM1704_logic is 
-- width: How many bits (from MSB) are gathered from the serial I2S input
generic(width : integer := 32);

port(
	--  PCM1704 ports
	BCLK      : out std_logic;      --Bit clock
	WCLK      : out std_logic;      --Word Clock
	DOUT      : out std_logic;      --Data Output
	
	-- Control ports
	CLK       : in  std_logic;      --Bit clock input
	RESET     : in  std_logic;      --Asynchronous Reset (Active Low)
	
	-- Parallel ports 
	-- use (width-1 downto 0); for big endian fotmat 
	-- or (0 to width-1) for little endian
	DATA_in     : in std_logic_vector(23 downto 0);   -- (MSB downto LSB)
	
	-- Status ports
	--DATA_RDY_out    : out std_logic;      --Falling edge means data is ready
	DATA_RDY_in     : in  std_logic     --Falling edge means data is ready
	
);
end PCM1704_logic;


architecture rtl of PCM1704_logic is

	--signals 
	signal counter           : integer range 0 to width;
	signal s_DATA_RDY_in     : std_logic;
	signal s_last_rdy        : std_logic;
	signal s_data            : std_logic_vector(0 to width-1);
		
begin
   
	BCLK <= CLK;
	
	-- serial to parallel interface
	PCM1704_logic: process(RESET, CLK, DATA_in, DATA_RDY_in)
	begin
		if(RESET = '1') then
			
			counter          <=  0;
			DOUT             <= '0';
			WCLK             <= '0';
			s_DATA_RDY_in    <= '0';
			s_data           <= (others => '0');

		elsif(CLK'event and CLK = '0') then		
		

			-- if there is a change from 0 to 1 in the DATA_RDY_in signal 
			-- reset the counter, set the last DOUT and copy the new DATA_in.
			if(DATA_RDY_in = '1' and s_last_rdy = '0') then
				DOUT       <=  s_data(counter);
				counter    <=  0;
				-- the PCM1704 converts the last 24 BIT?s to the analog value 
				-- so the data_in(0 to 23) from the buffer has to copied to
				-- the internal buffer s_data(8 to 31)
				s_data(8)      <=  DATA_in(23);
				s_data(9)      <=  DATA_in(22);
				s_data(10)     <=  DATA_in(21);
				s_data(11)     <=  DATA_in(20);
				s_data(12)     <=  DATA_in(19);
				s_data(13)     <=  DATA_in(18);
				s_data(14)     <=  DATA_in(17);
				s_data(15)     <=  DATA_in(16);
				s_data(16)     <=  DATA_in(15);
				s_data(17)     <=  DATA_in(14);
				s_data(18)     <=  DATA_in(13);
				s_data(19)     <=  DATA_in(12);
				s_data(20)     <=  DATA_in(11);
				s_data(21)     <=  DATA_in(10);
				s_data(22)     <=  DATA_in(9);
				s_data(23)     <=  DATA_in(8);
				s_data(24)     <=  DATA_in(7);
				s_data(25)     <=  DATA_in(6);
				s_data(26)     <=  DATA_in(5);
				s_data(27)     <=  DATA_in(4);
				s_data(28)     <=  DATA_in(3);
				s_data(29)     <=  DATA_in(2);
				s_data(30)     <=  DATA_in(1);
				s_data(31)     <=  DATA_in(0);
			else
				DOUT <= s_data(counter);
				counter <= counter + 1;
			end if; --(DATA_RDY_in = '1')
			
			if(counter = width-1) then
				WCLK      <= '1';
				counter   <=  0;  -- allows WCLK with oversampling
				-- allows to refresh the oversampled data input
				s_data(8)      <=  DATA_in(23);
                s_data(9)      <=  DATA_in(22);
                s_data(10)     <=  DATA_in(21);
                s_data(11)     <=  DATA_in(20);
                s_data(12)     <=  DATA_in(19);
                s_data(13)     <=  DATA_in(18);
                s_data(14)     <=  DATA_in(17);
                s_data(15)     <=  DATA_in(16);
                s_data(16)     <=  DATA_in(15);
                s_data(17)     <=  DATA_in(14);
                s_data(18)     <=  DATA_in(13);
                s_data(19)     <=  DATA_in(12);
                s_data(20)     <=  DATA_in(11);
                s_data(21)     <=  DATA_in(10);
                s_data(22)     <=  DATA_in(9);
                s_data(23)     <=  DATA_in(8);
                s_data(24)     <=  DATA_in(7);
                s_data(25)     <=  DATA_in(6);
                s_data(26)     <=  DATA_in(5);
                s_data(27)     <=  DATA_in(4);
                s_data(28)     <=  DATA_in(3);
                s_data(29)     <=  DATA_in(2);
                s_data(30)     <=  DATA_in(1);
                s_data(31)     <=  DATA_in(0);
			else
				WCLK      <= '0';
			end if;
					
			s_last_rdy <= DATA_RDY_in;
				
				
		end if; --(CLK'event and CLK = '0')
	end process PCM1704_logic;

end rtl;
